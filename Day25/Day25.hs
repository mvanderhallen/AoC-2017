{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import qualified Puzzle as Puzzle

  import Data.List (foldl')
  import Debug.Trace (trace)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = (St, Integer, [(St, Transition)])
  type Effect = (Integer, Direction, St)
  type Transition = (Effect, Effect)
  data Direction = L | R deriving (Eq, Show)
  data St = A | B | C | D | E | F deriving (Eq, Show)

  inputParser :: Parser PuzzleType
  inputParser = do
    bSt <- symbol "Begin in state" *> parseState <* symbol "."
    diag <- symbol "Perform a diagnostic checksum after" *> integer <* symbol "steps."
    ts <- many1 parseTransition
    return (bSt, diag, ts)
    where
      parseState :: Parser St
      parseState = return A <* symbol "A"
                <|> return B <* symbol "B"
                <|> return C <* symbol "C"
                <|> return D <* symbol "D"
                <|> return E <* symbol "E"
                <|> return F <* symbol "F"
      parseDirection :: Parser Direction
      parseDirection = (return L <* symbol "left")
                    <|> (return R <* symbol "right")
      parseEffect :: Parser Effect
      parseEffect = do
        v <- fromIntegral <$> (symbol "- Write the value " *> integer <* symbol ".")
        d <- symbol "- Move one slot to the " *> parseDirection <* symbol "."
        s <- symbol "- Continue with state " *> parseState <* symbol "."
        return (v,d,s)
      parseTransition :: Parser (St, Transition)
      parseTransition = do
        st <- symbol "In state " *> parseState <* symbol ":"
        symbol "If the current value is 0:"
        f1 <- parseEffect
        symbol "If the current value is 1:"
        f2 <- parseEffect
        return (st, (f1,f2))

  type Tape = ([Integer], [Integer])

  step :: [(St, Transition)] -> (St, Tape) -> (St, Tape)
  step tm (st, (lt, v:rt)) = case lookup st tm of
    Nothing      -> error ""
    Just (f1,f2) -> if v == 0 then
        execute f1 lt rt
      else
        execute f2 lt rt
    where
      ensure xs
        | null xs = [0]
        | otherwise = xs
      execute :: Effect -> [Integer] -> [Integer] -> (St, Tape)
      execute (v', L, s') lt rt = let v'':lt' = ensure lt in (s', (lt', v'':v':rt))
      execute (v', R, s') lt rt = (s', (v':lt, ensure rt))

  assignment1 :: PuzzleType -> Int
  assignment1 (beginState, diagnostic, tm) = countOnes $ snd $ foldl' (\acc _ -> step tm acc) (beginState, ([],[0])) [0..diagnostic-1]
    where
      countOnes :: Tape -> Int
      countOnes (lt,rt) = length $ filter (==1) $ lt ++ rt

  assignment2 :: PuzzleType -> Int
  assignment2 input = 1

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
