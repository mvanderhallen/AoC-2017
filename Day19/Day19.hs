{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq
  import Prelude hiding (Left, Right)

  import Data.List
  import Data.Maybe

  import Debug.Trace

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [[Char]]
  data Direction = Up | Down | Left | Right deriving (Eq, Show)

  parseInput :: Parser [[Char]]
  parseInput = many1 ((many1 parseChar) <* oneOf "\n")
    where
      parseChar = oneOf " -|+ABCDEFGHIJKLMNOPQRSTUVWXYZ"

  start :: PuzzleType -> (Int,Int)
  start grid = (fromJust $ elemIndex '|' $ head grid, 0)

  assignment1 grid = trace (step grid [] (start grid) Down) 0

  setStep :: (Int, Int) -> Direction -> (Int, Int)
  setStep (x,y) Up = (x,y-1)
  setStep (x,y) Down = (x,y+1)
  setStep (x,y) Left = (x-1,y)
  setStep (x,y) Right = (x+1,y)

  step :: PuzzleType -> [Char] -> (Int, Int) -> Direction -> [Char]
  step grid = go
    where
      go :: [Char] -> (Int, Int) -> Direction -> [Char]
      go acc (x,y) dir =
        case grid!!y!!x of
          '|' -> go acc (setStep (x,y) dir) dir
          '-' -> go acc (setStep (x,y) dir) dir
          '+' -> let dir' = decideDir grid dir (x,y) in
                 go acc (setStep (x,y) dir') dir'
          ' ' -> reverse acc
          c   -> go (c : acc) (setStep (x,y) dir) dir

  decideDir :: PuzzleType -> Direction -> (Int, Int) -> Direction
  decideDir grid dir (x,y)
    | dir == Up || dir == Down  = if x > 0 && (grid !! y !! (x -1)) /= ' ' then Left else Right
    | dir == Left || dir == Right = if y > 0 && (grid !! (y-1) !! x) /= ' ' then Up else Down

  assignment2 grid = step' grid 0 (start grid) Down

  step' :: PuzzleType -> Int -> (Int, Int) -> Direction -> Int
  step' grid = go
    where
      go :: Int -> (Int, Int) -> Direction -> Int
      go acc (x,y) dir =
        case grid!!y!!x of
          '|' -> go (acc+1) (setStep (x,y) dir) dir
          '-' -> go (acc+1) (setStep (x,y) dir) dir
          '+' -> let dir' = decideDir grid dir (x,y) in
                 go (acc+1) (setStep (x,y) dir') dir'
          ' ' -> acc
          c   -> go (acc+1) (setStep (x,y) dir) dir

  instance Puzzle.Puzzle PuzzleType where
    parseInput = parseInput
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType
