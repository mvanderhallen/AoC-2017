{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import qualified Puzzle as Puzzle

  import Data.List (delete, maximumBy)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Bridge]
  type Bridge = (Int, Int)



  inputParser :: Parser PuzzleType
  inputParser = many parseBridge
    where
    parseBridge :: Parser Bridge
    parseBridge = lexeme $ (,) <$> (fromIntegral <$> integer <* symbol "/") <*> (fromIntegral <$> integer)

  strength :: [Bridge] -> Int
  strength = sum . map (\(x,y) -> x + y)

  extendMax :: [Bridge] -> [Bridge] -> Int -> Int
  extendMax bridges xs end = case filter (eitherEq end) bridges of
    [] -> strength xs
    l -> maximum $ map (go) l
    where
      go :: Bridge -> Int
      go bridge = extendMax (delete bridge bridges) (bridge:xs) (other bridge end)
      other :: Bridge -> Int -> Int
      other (x,y) z = if (x==y) then x else if (x==z) then y else x
      eitherEq :: Int -> Bridge -> Bool
      eitherEq z (x,y)
        | x == z = True
        | y == z = True
        | otherwise = False

  extendLongest :: [Bridge] -> [Bridge] -> Int -> ([Bridge], Int)
  extendLongest bridges xs end = case filter (eitherEq end) bridges of
    [] -> (xs, length xs)
    l -> maximumBy order $ map (go) l
    where
      order :: ([Bridge], Int) -> ([Bridge], Int) -> Ordering
      order (b,l) (b', l') = case compare l l' of
        EQ   -> compare (strength b) (strength b')
        comp -> comp
      go :: Bridge -> ([Bridge], Int)
      go bridge = extendLongest (delete bridge bridges) (bridge:xs) (other bridge end)
      other :: Bridge -> Int -> Int
      other (x,y) z = if (x==y) then x else if (x==z) then y else x
      eitherEq :: Int -> Bridge -> Bool
      eitherEq z (x,y)
        | x == z = True
        | y == z = True
        | otherwise = False

  assignment1 :: PuzzleType -> Int
  assignment1 input = extendMax input [] 0

  assignment2 :: PuzzleType -> Int
  assignment2 input = strength $ fst $ extendLongest input [] 0

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
