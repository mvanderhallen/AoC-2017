{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.IntMap (IntMap)
  import qualified Data.IntMap as IM
  import Data.Maybe (fromJust, fromMaybe)
  import Control.Monad.State

  import Prelude hiding (Left, Right)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [[Bool]]

  type InfectionMap = IntMap Status
  data Status = Clean | Weakened | Infected | Flagged deriving (Show, Eq)
  data Direction = Up | Down | Left | Right deriving (Show, Eq)
  type Position = (Int, Int)
  data CarrierState = CarrierState { pos :: Position, dir :: Direction } deriving (Show, Eq)

  data InfectionState = InfectionState {im :: InfectionMap, infections :: Int, cs :: CarrierState, originals :: [Int]} deriving (Show, Eq)

  rotate :: State InfectionState ()
  rotate = do
    is@(InfectionState {..}) <- get
    case fromMaybe (Clean) $ im IM.!? (toSingle $ pos cs) of
      Clean    -> put $ is { cs = cs { dir = ccw $ dir cs }}
      Infected -> put $ is { cs = cs { dir = cw $ dir cs }}
      Flagged  -> put $ is { cs = cs { dir = flipped $ dir cs}}
      Weakened -> return ()
    where
      cw Up     = Right
      cw Right  = Down
      cw Down   = Left
      cw Left   = Up
      ccw Up    = Left
      ccw Left  = Down
      ccw Down  = Right
      ccw Right = Up
      flipped Up = Down
      flipped Right = Left
      flipped Down = Up
      flipped Left = Right

  infect :: State InfectionState ()
  infect = do
      is@(InfectionState {..}) <- get
      case im IM.!? (toSingle $ pos cs) of
        Nothing      -> put (InfectionState (IM.insert (toSingle $ pos cs) Infected im) (infections+1) cs originals)
        (Just Clean) -> put (InfectionState (IM.insert (toSingle $ pos cs) Infected im) (infections+1) cs originals)
        (Just Infected)  -> put (InfectionState (IM.insert (toSingle $ pos cs) Clean im) (infections) cs originals)

  move :: State InfectionState ()
  move = do
    is@(InfectionState {..}) <- get
    put (is { cs = move' cs})
    where
      move' :: CarrierState -> CarrierState
      move' (CarrierState pos dir) = CarrierState (go pos dir) dir
        where
          go (x,y) Up = (x,y-1)
          go (x,y) Right = (x+1,y)
          go (x,y) Down = (x,y+1)
          go (x,y) Left = (x-1,y)

  toSingle :: (Int, Int) -> Int
  toSingle (x,y) = (10000+ x)*100000 + (10000+y)

  toPos :: Int -> (Int, Int)
  toPos v = ((v `div` 100000) - 10000, (v `mod` 100000) - 10000)

  inputParser :: Parser [[Bool]]
  inputParser = parseInfectionMap
    where
      parseInfectionMap :: Parser [[Bool]]
      parseInfectionMap = many1 parseLine
      parseLine :: Parser [Bool]
      parseLine = lexeme $ many1 parseState
      parseState :: Parser Bool
      parseState = oneOf "." *> return False
                <|> oneOf "#" *> return True

  toInfectionMap :: [[Bool]] -> InfectionMap
  toInfectionMap m = IM.fromList [(toSingle (x-cx, y-cy), Infected) | x<-[0..w-1], y<-[0..h-1], m!!y!!x]
    where
      h = length m
      w = length $ m!!0
      cx = w `div` 2
      cy = h `div` 2

  toInfectionMap' :: [[Bool]] -> [((Int, Int), Bool)]
  toInfectionMap' m = [((x-cx, y-cy), True) | x<-[0..w-1], y<-[0..h-1], m!!y!!x]
    where
      h = length m
      w = length $ m!!0
      cx = w `div` 2
      cy = h `div` 2

  step :: State InfectionState ()
  step = rotate *> infect *> move

  assignment1 :: PuzzleType -> Int
  assignment1 input = infections $ execState go (InfectionState im' 0 (CarrierState (0,0) Up) (map fst $ IM.toList im'))
    -- filter (id . snd) $ map (\(x,y) -> (toPos x, y)) $ IM.toList $ im $ execState go (InfectionState im' 0 (CarrierState (0,0) Up) (map fst $ IM.toList im'))
    where
      n = 10000
      im' = toInfectionMap input
      go = mapM_ (\_ -> step) [0..n-1]
      -- go = step *> step

  infect' :: State InfectionState ()
  infect' = do
      is@(InfectionState {..}) <- get
      case im IM.!? (toSingle $ pos cs) of
        Nothing      -> put (InfectionState (IM.insert (toSingle $ pos cs) Weakened im) (infections) cs originals)
        (Just Clean) -> put (InfectionState (IM.insert (toSingle $ pos cs) Weakened im) (infections) cs originals)
        (Just Weakened)  -> put (InfectionState (IM.insert (toSingle $ pos cs) Infected im) (infections+1) cs originals)
        (Just Infected)  -> put (InfectionState (IM.insert (toSingle $ pos cs) Flagged im) (infections) cs originals)
        (Just Flagged)  -> put (InfectionState (IM.insert (toSingle $ pos cs) Clean im) (infections) cs originals)


  assignment2 :: PuzzleType -> Int
  assignment2 input = infections $ execState go (InfectionState im' 0 (CarrierState (0,0) Up) (map fst $ IM.toList im'))
    -- filter (id . snd) $ map (\(x,y) -> (toPos x, y)) $ IM.toList $ im $ execState go (InfectionState im' 0 (CarrierState (0,0) Up) (map fst $ IM.toList im'))
    where
      n = 10000000
      im' = toInfectionMap input
      go = mapM_ (\_ -> rotate *> infect' *> move) [0..n-1]

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType
