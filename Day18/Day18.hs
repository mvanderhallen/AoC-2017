{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Prelude hiding (lookup)
  import Data.List (sort, partition, (\\), nub, delete)
  import Data.HashMap.Strict (HashMap, fromList, (!), insert, adjust)
  import Data.Maybe (fromJust, isNothing)
  import Control.Monad.State
  import Control.Monad.Identity

  import System.IO.Unsafe

  import Debug.Trace (trace, traceM)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Operator]

  type Register = Char
  type Value = Either Register Integer
  data Operator = Snd Value | Set Register Value | Add Register Value | Mul Register Value
                | Mod Register Value | Rcv Register | Jgz Value Value deriving (Show, Eq)

  type Registers = HashMap Register Integer
  type Sent = [Integer]
  data OpState = OpState { registers :: Registers, sent :: Sent, result :: Maybe Integer} deriving (Show, Eq)

  inputParser :: Parser PuzzleType
  inputParser = many1 parseOperator
    where
    parseOperator  =  try (Snd <$> (symbol "snd" *> parseValue))
                  <|> try (Set <$> (symbol "set" *> parseRegister) <*> parseValue)
                  <|> try (Add <$> (symbol "add" *> parseRegister) <*> parseValue)
                  <|> try (Mul <$> (symbol "mul" *> parseRegister) <*> parseValue)
                  <|> try (Mod <$> (symbol "mod" *> parseRegister) <*> parseValue)
                  <|> try (Rcv <$> (symbol "rcv" *> parseRegister))
                  <|> try (Jgz <$> (symbol "jgz" *> parseValue) <*> parseValue)
    parseRegister = lexeme $ oneOf "abcdefghijklmnopqrstuvwxyz"
    parseValue = Left <$> try parseRegister
              <|> Right <$> integer

  initRegisters :: Registers
  initRegisters = fromList $ map (,0) "abcdefghijklmnopqrstuvwxyz"

  compute :: PuzzleType -> Int -> State OpState ()
  compute ops i
    | i < 0 || i >= length ops = undefined
    | otherwise = do
        pointerJump <- go $ ops !! i
        currState <- get
        when (isNothing $ result $ currState) $ case pointerJump of
          Nothing -> compute ops (i + 1)
          Just di -> compute ops (i + di)
      where
        go :: Operator -> State OpState (Maybe Int)
        go (Snd reg)   = do
          val <- eval reg
          modify (send val) *> return Nothing
        go (Set reg i) = do
          val <- eval i
          modify (alterReg reg (const val)) *> return Nothing
        go (Add reg i) = do
          val <- eval i
          modify (alterReg reg (+val)) *> return Nothing
        go (Mul reg i) = do
          val <- eval i
          modify (alterReg reg (*val)) *> return Nothing
        go (Mod reg i) = do
          val <- eval i
          modify (alterReg reg (\x -> x `mod` val)) *> return Nothing
        go (Rcv reg)   = do
          v <- eval (Left reg)
          if v /= 0 then do
            modify (setResult)
            modify (recover reg)
            return Nothing
          else
            return Nothing
        go (Jgz c i) = do
          check <- eval c
          val <- eval i
          if (check > 0) then do
            return $ Just $ fromIntegral val
          else
            return Nothing
  eval :: Value -> State OpState Integer
  eval (Left r) = do
    regs <- (gets registers)
    return $ regs ! r
  eval (Right v) = return v

  send :: Integer -> OpState -> OpState
  send val s@(OpState{..}) = s {sent = (val:sent) }

  set :: Register -> Integer -> OpState -> OpState
  set reg val s@(OpState{..}) = s { registers = insert reg val registers }

  alterReg :: Register -> (Integer -> Integer) -> OpState -> OpState
  alterReg reg f s@(OpState{..}) = s { registers = adjust f reg registers }

  recover :: Register -> OpState -> OpState
  recover reg s@(OpState{..}) =
    let val = head sent in
        set reg val s

  setResult :: OpState -> OpState
  setResult s@(OpState{..}) = s { result = Just $ maybe (head sent) id result}

  assignment1 :: PuzzleType -> Integer
  assignment1 ops = fromJust $ result $ execState (compute ops 0) (OpState initRegisters [] Nothing)

  data IOResult = SEND Integer | RECEIVE Register | TERMINATE deriving (Show, Eq)
  data IOState  = Reading Register | Runnable | Terminated deriving (Show, Eq)
  data Process = P0 | P1 deriving (Show, Eq)
  data ProcessState = Process { pointer :: Int, io :: IOState, opState :: OpState } deriving (Show, Eq)
  data CommunicationBus = CommunicationBus { p0ToP1 :: ([Integer], Int), p1ToP0 :: ([Integer], Int)} deriving (Show, Eq)
  type Pointer = Int

  awaits :: Process -> CommunicationBus -> Bool
  awaits P0 cb = let (ls,l) = p1ToP0 cb in
    if l == length ls then False else True
  awaits P1 cb = let (ls,l) = p0ToP1 cb in
    if l == length ls then False else True

  readCB :: Process -> CommunicationBus -> (CommunicationBus, Integer)
  readCB P0 (CommunicationBus{..})  = let (ls,l) = p1ToP0 in (CommunicationBus p0ToP1 (ls,l+1) , ls!!l)
  readCB P1 (CommunicationBus{..}) = let (ls,l) = p0ToP1 in (CommunicationBus (ls,l+1) p1ToP0  , ls!!l)

  writeCB :: Process -> Integer -> CommunicationBus -> CommunicationBus
  writeCB P0 val (CommunicationBus{..})  = let (ls,l) = p0ToP1 in CommunicationBus (ls++[val],l) p1ToP0
  writeCB P1 val (CommunicationBus{..}) = let (ls,l) = p1ToP0 in CommunicationBus p0ToP1  (ls++[val],l)

  sendsByTwo :: CommunicationBus -> Int
  sendsByTwo (CommunicationBus{..}) = length $ fst p1ToP0

  toProcessStateAndCB :: Process -> ((IOResult, Pointer), OpState) -> CommunicationBus -> (ProcessState, CommunicationBus)
  toProcessStateAndCB process ((SEND i, p), op) cb = (Process p Runnable op, writeCB process i cb)
  toProcessStateAndCB process ((RECEIVE r, p), op) cb = (Process p (Reading r) op, cb)
  toProcessStateAndCB process ((TERMINATE, p), op) cb = (Process p (Terminated) op, cb)

  runTwo :: PuzzleType -> Int
  runTwo ops = runIdentity $ runP0 (Process 0 Runnable $ stateFor 0) (Process 0 Runnable $ stateFor 1) (CommunicationBus ([],0) ([],0))
    where
      stateFor i = (OpState (insert 'p' i $ initRegisters) [] Nothing)
      runP0 :: ProcessState -> ProcessState -> CommunicationBus -> Identity Int
      runP0 p1@(Process{..}) p2 cb = do
        case io of
          Terminated ->
            runP1 p1 p2 cb
          Reading r  -> do
            if awaits P0 cb then do
              let (cb', val) = readCB P0 cb
              let result = runState (runUntilIO P0 pointer (Just $ Set r (Right val))) $ opState
              let (p1', cb'') = toProcessStateAndCB P0 result cb'
              runP0 p1' p2 cb''
            else do
              runP1 p1 p2 cb
          Runnable -> do
            let result = runState (runUntilIO P0 pointer Nothing) $ opState
            let (p1', cb') = toProcessStateAndCB P0 result cb
            runP0 p1' p2 cb'
      runP1 :: ProcessState -> ProcessState -> CommunicationBus -> Identity Int
      runP1 p1 p2@(Process{..}) cb = do
        case io of
          Terminated ->
            if cancontinue p1 cb then
              runP0 p1 p2 cb
            else
              return $ sendsByTwo cb
          Reading r  -> do
            if awaits P1 cb then do
              let (cb', val) = readCB P1 cb
              let result = runState (runUntilIO P1 pointer (Just $ Set r (Right val))) $ opState
              let (p2', cb'') = toProcessStateAndCB P1 result cb'
              runP0 p1 p2' cb''
            else if cancontinue p1 cb then do
              runP0 p1 p2 cb
            else do
              return $ sendsByTwo cb
          Runnable -> do
            let result = runState (runUntilIO P1 pointer Nothing) $ opState
            let (p2', cb') = toProcessStateAndCB P1 result cb
            runP0 p1 p2' cb'

      cancontinue :: ProcessState -> CommunicationBus -> Bool
      cancontinue (Process{..}) cb = case io of
        Terminated -> False
        Reading _  -> awaits P0 cb
        Runnable   -> True

      addToSends :: Integer -> Bool -> (([Integer], Int), ([Integer], Int)) -> (([Integer], Int), ([Integer], Int))
      addToSends val True ((ls, p),x) = ((ls++[val],p),x)
      addToSends val False (x,(ls, p)) = (x,(ls++[val],p))

      runUntilIO :: Process -> Pointer -> Maybe Operator -> State OpState (IOResult, Pointer)
      runUntilIO p pointer (Just (Set reg i)) = do
        val <- eval i
        modify (alterReg reg (const val))
        runUntilIO p (pointer) Nothing
      runUntilIO p pointer Nothing = do
        if pointer < 0 || pointer >= length ops then
          return (TERMINATE, pointer)
        else
          case ops !! pointer of
            Snd reg -> do
              val <- eval reg
              return $ (SEND val, pointer + 1)
            Rcv reg -> do
              return $ (RECEIVE reg, pointer + 1)
            Set reg i -> do
              val <- eval i
              modify (alterReg reg (const val))
              runUntilIO p (pointer + 1) Nothing
            Add reg i -> do
              val <- eval i
              modify (alterReg reg (+val))
              runUntilIO p (pointer + 1) Nothing
            Mul reg i -> do
              val <- eval i
              modify (alterReg reg (*val))
              runUntilIO p (pointer + 1) Nothing
            Mod reg i -> do
              val <- eval i
              modify (alterReg reg (\x -> x `mod` val))
              runUntilIO p (pointer + 1) Nothing
            Jgz c i -> do
              check <- eval c
              val <- eval i
              if (check > 0) then do
                runUntilIO p (pointer + fromIntegral val) Nothing
              else
                runUntilIO p (pointer + 1) Nothing

  assignment2 :: PuzzleType -> Int
  assignment2 ops = runTwo ops

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . fromIntegral . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
