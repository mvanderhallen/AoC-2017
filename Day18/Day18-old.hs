{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Prelude hiding (lookup)
  import Data.List (sort, partition, (\\), nub, delete)
  import Data.HashMap.Strict (HashMap, fromList, (!), insert, adjust)
  import Data.Maybe (fromJust)
  import Control.Monad.State
  import Control.Monad.Identity

  import Debug.Trace (trace, traceM)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Operator]

  type Register = Char
  type Value = Either Register Integer
  data Operator = Snd Value | Set Register Value | Add Register Value | Mul Register Value
                | Mod Register Value | Rcv Register | Jgz Register Value deriving (Show, Eq)

  type Registers = HashMap Register Integer
  type Sent = [Integer]
  data OpState = OpState { registers :: Registers, sent :: Sent, jumped :: Bool} deriving (Show, Eq)

  inputParser :: Parser PuzzleType
  inputParser = many1 parseOperator
    where
    parseOperator  =  try (Snd <$> (symbol "snd" *> parseValue))
                  <|> try (Set <$> (symbol "set" *> parseRegister) <*> parseValue)
                  <|> try (Add <$> (symbol "add" *> parseRegister) <*> parseValue)
                  <|> try (Mul <$> (symbol "mul" *> parseRegister) <*> parseValue)
                  <|> try (Mod <$> (symbol "mod" *> parseRegister) <*> parseValue)
                  <|> try (Rcv <$> (symbol "rcv" *> parseRegister))
                  <|> try (Jgz <$> (symbol "jgz" *> parseRegister) <*> parseValue)
    parseRegister = lexeme $ oneOf "abcdefghijklmnopqrstuvwxyz"
    parseValue = Left <$> try parseRegister
              <|> Right <$> integer

  initRegisters :: Registers
  initRegisters = fromList $ map (,0) "abcdefghijklmnopqrstuvwxyz"

  compute :: PuzzleType -> Int -> State OpState ()
  compute ops i
    | i < 0 || i >= length ops = undefined
    | otherwise = do
        result <- go $ ops !! i
        case result of
          Nothing -> compute ops (i + 1)
          Just di -> compute ops (i + di)
      where
        go :: Operator -> State OpState (Maybe Int)
        go (Snd reg)   = do
          val <- eval reg
          modify (send val) *> return Nothing
        go (Set reg i) = do
          val <- eval i
          modify (alterReg reg (const val)) *> return Nothing
        go (Add reg i) = do
          val <- eval i
          modify (alterReg reg (+val)) *> return Nothing
        go (Mul reg i) = do
          val <- eval i
          modify (alterReg reg (*val)) *> return Nothing
        go (Mod reg i) = do
          val <- eval i
          modify (alterReg reg (\x -> x `mod` val)) *> return Nothing
        go (Rcv reg)   = modify (recover reg) *> return Nothing
        go (Jgz reg i) = do
          val <- eval i
          check <- eval (Left reg)
          if (check /= 0) then do
            modify (\OpState{..} -> OpState registers sent True)
            return $ Just $ fromIntegral val
          else
            return Nothing
  eval :: Value -> State OpState Integer
  eval (Left r) = do
    regs <- (gets registers)
    return $ regs ! r
  eval (Right v) = return v

  send :: Integer -> OpState -> OpState
  send val (OpState{..}) = OpState registers (val:sent) jumped
  set :: Register -> Integer -> OpState -> OpState
  set reg val (OpState{..}) = OpState (insert reg val registers) sent jumped
  alterReg :: Register -> (Integer -> Integer) -> OpState -> OpState
  alterReg reg f (OpState{..}) = OpState (adjust f reg registers) sent jumped
  recover :: Register -> OpState -> OpState
  recover reg s@(OpState{..}) = let val = head sent in
    trace (show (jumped, val)) $
      if (val /= 0) then
        set reg val s
      else
        s

  assignment1 :: PuzzleType -> _
  assignment1 ops = runState (compute ops 0) (OpState initRegisters [] False)

  data IOResult = SEND Integer | RECEIVE Register deriving (Show, Eq)
  data IOState  = Reading Reg | Runnable | Terminated deriving (Show, Eq)
  data Process = P0 | P1 deriving (Show, Eq)
  data ProcessState = Process { pointer :: Int, io :: Maybe FIO, opState :: OpState } deriving (Show, Eq)
  data CommunicationBus = CommunicationBus { p1ToP2 :: ([Integer], Int), p2ToP1 :: ([Integer], Int)} deriving (Show, Eq)

  awaits :: Process -> CommunicationBus -> Bool
  awaits P0 cb = let (ls,l) = p1ToP2 cb in
    if l == length ls then False else True
  awaits P1 cb = let (ls,l) = p2ToP1 cb in
    if l == length ls then False else True

  readCB :: Process -> CommunicationBus -> (CommunicationBus, Integer)
  readCB P0 (CommunicationBus{..})  = let (ls,l) = p1ToP2 in (CommunicationBus (ls,l+1) p2ToP1 , ls!!l)
  readCB P1 (CommunicationBus{..}) = let (ls,l) = p2ToP1 in (CommunicationBus p1ToP2  (ls,l+1), ls!!l)

  writeCB :: Process -> Integer -> CommunicationBus -> CommunicationBus
  writeCB P0 val (CommunicationBus{..})  = let (ls,l) = p1ToP2 in CommunicationBus (ls++[val],l) p2ToP1
  writeCB P1 val (CommunicationBus{..}) = let (ls,l) = p2ToP1 in CommunicationBus p1ToP2  (ls++[val],l)

  sendsByTwo :: CommunicationBus -> Int
  sendsByTwo (CommunicationBus{..}) = length $ fst p2ToP1



  runTwo :: PuzzleType -> Int
  runTwo ops = runIdentity $ go (Process 0 Nothing $ stateFor 0) (Process 0 Nothing $ stateFor 1) (CommunicationBus ([],0) ([],0))
    where
      stateFor i = (OpState (insert 'p' i $ initRegisters) [] False)
      go :: ProcessState -> ProcessState -> CommunicationBus -> Identity Int
      go p1 p2 cb = do
        traceM (show p1 ++ " | " ++ show p2)
        case io p1 of
          Nothing ->
            case runState (runUntilIO (pointer p1) Nothing) $ opState p1 of
              ((Just (SEND v), p), ops) -> go (Process p Nothing ops) p2 (writeCB True v cb)
              ((mfio, p), ops)          -> go (Process p mfio ops) p2 cb
          Just (RECEIVE reg) ->
            if awaits True  cb then do
              let (cb', val) = readCB True cb
              case runState (runUntilIO (pointer p1) (Just $ Set reg (Right val))) $ opState p1 of
                ((Just (SEND v), p), ops) -> go (Process p Nothing ops) p2 (writeCB True v cb)
                ((mfio, p), ops)          -> go (Process p mfio ops) p2 cb
            else
              case io p2 of
                Nothing ->
                  case runState (runUntilIO (pointer p2) Nothing) $ opState p2 of
                    ((Just (SEND v), p), ops) -> go p1 (Process p Nothing ops) (writeCB False v cb)
                    ((mfio, p), ops)          -> go p1 (Process p mfio ops) cb
                Just (RECEIVE reg) ->
                  if awaits False cb then do
                    let (cb', val) = readCB False cb
                    case runState (runUntilIO (pointer p2) (Just $ Set reg (Right val))) $ opState p2 of
                      ((Just (SEND v), p), ops) -> go p1 (Process p Nothing ops) (writeCB False v cb)
                      ((mfio, p), ops)          -> go p1 (Process p mfio ops) cb
                  else
                    return $ sendsByTwo cb
      -- go prog1 (p, Just (Send i), s) _ = undefined
      -- go (p, Just (SEND i), s) prog2 _ = undefined
      -- go (p, Just (RECEIVE r), s) p2 _ =
      -- go (_,(Just (RECEIVE _)),_) (_, (Just (RECEIVE _)), _) sends = length $ fst $ fst sends
      -- go (p1, Nothing, s1) (p2, mo2, s2) sends =
      --   let ((mo1',p1'),s1') = flip runState s1 (runUntilIO p1 Nothing)
      --   in go (p1',mo1',s1') (p2,mo2,s2) sends
      -- go (p1, mo1, s1) (p2, Nothing, s2) sends =
      --   let ((mo2', p2'),s2') = flip runState s2 (runUntilIO p2 Nothing)
      --   in go (p1,mo1,s1) (p2',mo2',s2') sends
      -- go (p1, (Just (RECEIVE r)),s1) (p2, (Just (SEND v)), s2) t =
      --   let ((mo1', p1'),s1') = flip runState s1 (runUntilIO p1 $ Just $ Add r (Right v))
      --   in go (p1',mo1',s1') (p2, Nothing, s2) (t+1)
      -- go (p1, (Just (SEND v)),s1) (p2, (Just (RECEIVE r)), s2)  =
      --   let ((mo2', p2'),s2') = flip runState s2 (runUntilIO p2 $ Just $ Add r (Right v))
      --   in go (p1,Nothing,s1) (p2', mo2', s2') t

      addToSends :: Integer -> Bool -> (([Integer], Int), ([Integer], Int)) -> (([Integer], Int), ([Integer], Int))
      addToSends val True ((ls, p),x) = ((ls++[val],p),x)
      addToSends val False (x,(ls, p)) = (x,(ls++[val],p))

      runUntilIO :: Int -> Maybe Operator -> State OpState (Maybe FIO, Int)
      runUntilIO pointer (Just (Set reg i)) = do
        val <- eval i
        modify (alterReg reg (const val))
        runUntilIO (pointer) Nothing
      runUntilIO pointer Nothing = do
        traceM $ show pointer
        if pointer < 0 || pointer >= length ops then
          return (Just $ RECEIVE 'a', pointer)
        else
          case ops !! pointer of
            Snd reg -> do
              val <- eval reg
              return $ (Just $ SEND val, pointer + 1)
            Rcv reg -> do
              return $ (Just $ RECEIVE reg, pointer + 1)
            Set reg i -> do
              val <- eval i
              modify (alterReg reg (const val))
              runUntilIO (pointer + 1) Nothing
            Add reg i -> do
              val <- eval i
              modify (alterReg reg (+val))
              runUntilIO (pointer + 1) Nothing
            Mul reg i -> do
              val <- eval i
              modify (alterReg reg (*val))
              runUntilIO (pointer + 1) Nothing
            Mod reg i -> do
              val <- eval i
              modify (alterReg reg (\x -> x `mod` val))
              runUntilIO (pointer + 1) Nothing
            Jgz reg i -> do
              val <- eval i
              check <- eval (Left reg)
              if (check /= 0) then do
                modify (\OpState{..} -> OpState registers sent True)
              else
                return ()
              runUntilIO (pointer + 1) Nothing




  assignment2 :: PuzzleType -> Int
  assignment2 ops = runTwo ops

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = undefined assignment1
    assignment2 = assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
