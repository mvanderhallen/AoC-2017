{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, transpose, foldl')
  import Data.Maybe (fromJust)
  import qualified Data.IntMap as IM
  import Data.IntMap (IntMap)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [(Grid, Grid)]
  data Dimension = TwoByTwo | ThreeByThree | FourByFour deriving (Eq, Show)
  --

  type Grid = [[Bool]]

  toInt :: Grid -> Int
  toInt = foldl (\acc b -> acc * 32 + fromBinary b) 0

  fromBinary :: [Bool] -> Int
  fromBinary = foldl (\acc b -> acc*2 + if b then 1 else 0) 0

  rules :: [(Grid, Grid)] -> (IntMap Grid, IntMap Grid)
  rules = foldl (\acc (f,t) -> go f t acc) (IM.empty, IM.empty)
    where
      go :: Grid -> Grid -> (IntMap Grid, IntMap Grid) -> (IntMap Grid, IntMap Grid)
      go from to (twos, threes)
        | length from == 2 = (foldl (\acc (f,t) -> IM.insert (toInt f) t acc) twos $ zip (variations from) (repeat to), threes)
        | length from == 3 = (twos, foldl (\acc (f,t) -> IM.insert (toInt f) t acc) threes $ zip (variations from) (repeat to))

  rotate :: Grid -> Grid
  rotate = map reverse . transpose

  flipHorizontal   = map reverse

  flipVertical  = reverse

  variations :: Grid -> [Grid]
  variations g = rots ++ map (map reverse) rots ++ map reverse rots
    where
      rots = [g, rotate g, rotate $ rotate g, rotate $ rotate $ rotate g]

  art :: (IntMap Grid, IntMap Grid) -> Grid -> Grid
  art (twos, threes) grid = join (map (\x -> relMap IM.! (toInt x)) <$> chunked size grid)
    where
      size = if length grid `mod` 2 == 0 then 2 else 3
      relMap = if (length grid `mod` 2 == 0) then twos else threes

  chunked :: forall a. Int -> [[a]] -> [[[[a]]]]
  chunked size grid = map go $ chunksOf size grid
    where
      go :: forall b. [[b]] -> [[[b]]]
      go g = map (reverse) $ foldl' (\acc v-> zipWith (:) v acc) [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]] $ map (chunksOf size) g --chunksOf 2
      -- size = if length grid `mod` 2 == 0 then 2 else 3
      -- map = if (length grid `mod` 2 == 0) then twos else threes

  grid = [[1, 2, 3, 4],[5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]

  build :: ((a -> [a] -> [a]) -> [a] -> [a]) -> [a]
  build g = g (:) []

  chunksOf :: Int -> [e] -> [[e]]
  chunksOf i ls = map (take i) (build (splitter ls)) where
    splitter :: [e] -> ([e] -> a -> a) -> a -> a
    splitter [] _ n = n
    splitter l c n  = l `c` splitter (drop i l) c n

  join :: [[Grid]] -> Grid
  join = joinVerticals . map (joinHorizontals)

  joinVerticals :: [Grid] -> Grid
  joinVerticals = concat

  joinHorizontals :: [Grid] -> Grid
  joinHorizontals = foldl joinHorizontal [[],[],[],[]]

  joinHorizontal :: Grid -> Grid ->  Grid
  joinHorizontal = zipWith (++)

  inputParser :: Parser PuzzleType
  inputParser = many1 rule
    where
      parseBool :: Parser Bool
      parseBool = oneOf "." *> return False
               <|> oneOf "#" *> return True
      parseLine :: Parser [Bool]
      parseLine = many1 parseBool
      parseFull :: Parser Grid
      parseFull = sepBy1 parseLine (oneOf "/")
      rule :: Parser (Grid, Grid)
      rule = do
        g1 <- lexeme $ parseFull
        symbol "=>"
        g2 <- lexeme $ parseFull
        return (g1,g2)


  assignment1 :: PuzzleType -> Int
  assignment1 input = length $ filter id $ concat $ foldl (\acc _ -> art relRules acc) [[False,True,False],[False, False, True], [True, True, True]] $ [0..4]
    where
      relRules = rules input

  assignment2 :: PuzzleType -> Int
  assignment2 input = length $ filter id $ concat $ foldl (\acc _ -> art relRules acc) [[False,True,False],[False, False, True], [True, True, True]] $ [0..17]
    where
      relRules = rules input


  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  printGrid :: Grid -> IO ()
  printGrid = putStrLn . unlines . map (map (go))
    where
      go True = '#'
      go False = '.'
