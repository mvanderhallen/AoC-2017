{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import Data.List (insert)
  import Control.Monad.State
  import Data.Maybe (fromMaybe)

  import qualified Puzzle as Puzzle

  import System.IO.Unsafe (unsafePerformIO)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Operator]

  inputParser :: Parser PuzzleType
  inputParser = many1 (lexeme parseOperator)
    where
      parseOperator :: Parser Operator
      parseOperator = Set <$> ((try $ symbol "set") *> parseRegister) <*> parseValue
                   <|> Sub <$> ((try $ symbol "sub") *> parseRegister) <*> parseValue
                   <|> Mul <$> ((try $ symbol "mul") *> parseRegister) <*> parseValue
                   <|> Jnz <$> ((try $ symbol "jnz") *> parseValue) <*> parseValue
      parseRegister :: Parser Register
      parseRegister = lexeme $ oneOf "abcdefgh"
      parseValue :: Parser Value
      parseValue = Left <$> parseRegister
                <|> (Right . fromIntegral) <$> integer

  type Register = Char
  type Value = Either Register Int
  data Operator = Set Register Value | Sub Register Value | Mul Register Value | Jnz Value Value deriving (Eq, Show)
  data CoprocessorState = CoprocessorState { reg :: [(Char, Int)], mults :: Int, pc :: Int } deriving (Eq, Show)

  class Monad m => Registry m where
    eval :: Value -> m Int
    write :: Register -> (Int -> Int) -> m ()

  class Monad m => Stepper m where
    programCounter :: (Int -> Int) -> m ()
    getCounter :: m Int

  class Monad m => MultTracker m where
    mult :: m ()

  type Coprocessor = State CoprocessorState

  instance Registry Coprocessor where
    eval (Right v) = return v
    eval (Left r) = do
      registry <- gets reg
      return $ fromMaybe 0 $ lookup r registry
    write r f = do
      cs@(CoprocessorState{..}) <- get
      let v = fromMaybe 0 $ lookup r reg
      put (cs { reg = insert (r,f v) $ filter (\(r',_) -> r /= r') reg })

  instance Stepper Coprocessor where
    programCounter f = do
      cs@(CoprocessorState{..}) <- get
      put (cs {pc = f pc})
    getCounter = gets pc
      -- seq (unsafePerformIO $ print pc') $ return pc'

  instance MultTracker Coprocessor where
    mult = do
      cs@(CoprocessorState{..}) <- get
      put (cs {mults = mults + 1})


  run :: (Registry m, MultTracker m, Stepper m) => [Operator] -> m ()
  run program = do
    programCounter <- getCounter
    if programCounter == 25 then do
      b <- eval (Left 'b')
      c <- eval (Left 'c')
      seq (unsafePerformIO $ print [b,c]) $ return ()
    else
      return ()
    if programCounter >=0 && programCounter < length program then do
      operate (program !! programCounter)
      run program
    else
      return ()


  operate :: (Registry m, MultTracker m, Stepper m) => Operator -> m ()
  operate (Set r v) = do
    val <- eval v
    write r (const val)
    programCounter (+1)
  operate (Sub r v) = do
    val <- eval v
    write r (\x -> x - val)
    programCounter (+1)
  operate (Mul r v) = do
    val <- eval v
    mult
    write r (* val)
    programCounter (+1)
  operate (Jnz v v') = do
    check <- eval v
    val <- eval v'
    if check /= 0 then
      programCounter (+ val)
    else
      programCounter (+1)

  assignment1 :: PuzzleType -> Int
  assignment1 input = mults $ execState (run input) $ CoprocessorState [] 0 0

  assignment2' :: PuzzleType -> CoprocessorState
  assignment2' input = execState (run input) $ CoprocessorState [('a',1)] 0 0

  assignment2 :: PuzzleType -> Int
  assignment2 _ = length [x | x <- [b, b+17..c], divis x]
    where
      b = 79*100+100000
      c = b + 17000
      divis :: Int -> Bool
      divis x = not $ null $ [y | y<-[2..x`div`2], x `mod` y == 0]

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType

  {- Puzzle Input
  -- b = 79*100+100000
  -- c = b + 17000;
  -- h = 0;
  --
  -- func computeH:
  --   f = 1;
  --   d = 2;
  --   while (d!=b) {
  --     e = 2;
  --     while (e!=b) {
  --       if ((d * e)-b == 0) {
  --          f = 0;
  --       }
  --       e++;
  --     }
  --     d++;
  --   }
  --   if (f == 0)
  --     h++;
  --   else if b == c
  --     return h;
  --   else
  --     b += 17;
  --     return computeH
  -}
