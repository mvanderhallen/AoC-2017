{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq
  import Data.IntMap.Strict (IntMap, empty, insert, (!), adjust, fromList, size)

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)
  import Data.Char
  import Control.Monad.Identity
  import Data.Function ((&))

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Command]

  data ProgramLine = MkProgramLine Int (IntMap Char) (IntMap Int)
  data Command = Spin Int | Exchange Int Int | Partner Char Char deriving Show

  instance Show ProgramLine where
    show (MkProgramLine offset im _) = map (im!) $ [offset..s] ++ [0..offset-1]
      where
        s = (size im)-1

  toChar = chr . (+ 97)
  fromChar = (+ (-97)) . ord

  inputParser :: Parser PuzzleType
  inputParser = sepBy1 parseCommand (symbol ",")
    where
      parseCommand =  (Spin <$> (symbol "s" *> parseNumber))
                  <|> (Exchange <$> (symbol "x" *> parseNumber) <*> (symbol "/" *> parseNumber))
                  <|> (Partner <$> (symbol "p" *> parseProgram) <*> (symbol "/" *> parseProgram))
      parseNumber = fromIntegral <$> natural
      parseProgram = oneOf "abcdefghijklmnop"

  initialLine :: [Char] -> ProgramLine
  initialLine progs = MkProgramLine 0 (fromList $ zip [0..] progs) (fromList $ zip (map (fromChar) progs) [0..])

  dance :: [Command] -> ProgramLine -> ProgramLine
  dance []     !p = p
  dance (c:cs) !p = dance cs $ case c of
    (Spin x)       -> spin x p
    (Exchange x y) -> exchange x y p
    (Partner x y)  -> partner x y p
    where
      spin x (MkProgramLine offset posToProg progToPos) =
        MkProgramLine (((offset+ll)-x) `mod` ll) posToProg progToPos
      exchange x y (MkProgramLine offset posToProg progToPos) = runIdentity $ do
        let pos1 = (offset+x) `mod` ll
        let prog1 = posToProg ! pos1
        let pos2 = (offset+y) `mod` ll
        let prog2 = posToProg ! pos2
        let posToProg' = adjust (\_->prog1) pos2 $ adjust (\_->prog2) pos1 posToProg
        let progToPos'  = adjust (\_->pos1) (fromChar prog2) $ adjust (\_ -> pos2) (fromChar prog1) progToPos
        return $ MkProgramLine offset posToProg' progToPos'
      partner x y (MkProgramLine offset posToProg progToPos) = runIdentity $ do
        let pos1  = progToPos ! (fromChar x)
        let pos2  = progToPos ! (fromChar y)
        let posToProg' = adjust (const x) pos2 $ adjust (const y) pos1 posToProg
        let progToPos' = adjust (const pos1) (fromChar y) $ adjust (const pos2) (fromChar x) progToPos
        return $ MkProgramLine offset posToProg' progToPos'
      ll = case p of
        MkProgramLine _ im _ -> size im

  assignment1 :: PuzzleType -> String
  assignment1 = show . flip dance (initialLine "abcdefghijklmnop")

  assignment2 :: PuzzleType -> String
  assignment2 commands = show $ foldr ($) (initialLine "abcdefghijklmnop") (replicate (1000000000 `mod` findCycleLength commands) $ dance commands)

  findCycleLength :: PuzzleType -> Int
  findCycleLength commands = go 1 $ initialLine "abcdefghijklmnop"
    where
      go x pl =
        if show pl' == "abcdefghijklmnop" then
          x
        else
          go (x+1) pl'
        where
          pl' = dance commands pl

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType
