{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TypeApplications #-}
module Puzzle where
  import System.Environment
  import Criterion.Main
  import Text.Parsec

  type Parser a = Parsec String () a

  class Show a => Puzzle a where
    parseInput  :: Parser a
    assignment1 :: a -> String
    assignment2 :: a -> String

    assignment1 _ = "Not Implemented"
    assignment2 _ = "Not Implemented"


  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  doMain :: forall a . Puzzle a => IO ()
  doMain = do
    args <- getArgs
    let fp = head args
    if (length $ filter (=="benchmark") args) == 1 then do
      withArgs (drop 2 args) $ criterion @a fp
    else do
      input <- getAOCInput parseInput fp :: IO a
      putStrLn $ assignment1 input
      putStrLn $ assignment2 input

  interactive :: forall a . Puzzle a => FilePath -> IO ()
  interactive fp = withArgs [fp] (doMain @ a)

  criterion :: forall a. Puzzle a => FilePath -> IO ()
  criterion fp = do
    input <- getAOCInput parseInput fp :: IO a
    print input
    defaultMain
      [
        bgroup "Gather input" $ [bench fp $ whnfIO (getAOCInput parseInput fp :: IO a)],
        bgroup "Assignment #1" $ [bench fp $ whnf assignment1 input],
        bgroup "Assignment #2" $ [bench fp $ whnf assignment2 input]
      ]
