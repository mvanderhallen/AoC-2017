{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.Sequence
  import qualified Data.Sequence as S
  import Data.List (foldl')

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = Int

  whirl c n = foldr (\v (p,s)-> (calcPointer p v c, insertAt (calcPointer p v c) v s)) (1, singleton 0) $ Prelude.reverse [1..n]

  calcPointer p v c = 1+(p+c) `mod` v

  assignment1 :: PuzzleType -> Int
  assignment1 c = case viewl $ S.drop 1 $ dropWhileL (/=2017) $ snd $ whirl c 2017 of
    (a :< _) -> a

  data SpinState = MkSpinState { pointer :: Int, posZero :: Int, successor :: Int} deriving Show

  assignment2 :: PuzzleType -> Int
  assignment2 c = successor $ foldl' go (MkSpinState 1 0 0) [1..50000000]
    where
      go :: SpinState -> Int -> SpinState
      go (MkSpinState{..}) v =
        if newPos <= posZero then
          MkSpinState newPos (posZero+1) successor
        else if newPos == posZero + 1 then
          MkSpinState newPos (posZero) v
        else
          MkSpinState newPos posZero successor
        where
          newPos = calcPointer pointer v c

  instance Puzzle.Puzzle PuzzleType where
    parseInput = fromIntegral <$> natural
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType
