{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq
  import Prelude hiding (Left, Right)

  import Data.List
  import Data.Maybe

  import Debug.Trace

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Point]
  type Vector = (Int, Int, Int)
  data Point = MkPoint { position :: Vector, velocity :: Vector, acceleration :: Vector } deriving (Show, Eq)

  -- instance Ord Vector where
    -- compare (x,y,z) (x',y',z') = last $ EQ : zipWith compare [x,y,z] [x',y',z']
  instance Num Vector where
    (+) (x,y,z) (dx,dy,dz) = (x+dx, y+dy, z+dz)
    (*) (x,y,z) (dx,dy,dz) = (x*dx, y*dy, z*dz)
    (-) (x,y,z) (dx,dy,dz) = (x-dx, y-dy, z-dz)
    abs (x,y,z) = (abs x, abs y, abs z)
    signum (x,y,z) = undefined
    fromInteger _ = undefined

  parseInput :: Parser [Point]
  parseInput = many1 parsePoint
    where
      parsePoint :: Parser Point
      parsePoint = do
        [pos, vel, acc] <- sepBy parseVector $ symbol ","
        return $ MkPoint pos vel acc
      parseVector = do
        oneOf "pva"
        symbol "="
        symbol "<"
        x <- fromIntegral <$> integer <* symbol ","
        y <- fromIntegral <$> integer <* symbol ","
        z <- fromIntegral <$> integer
        symbol ">"
        return (x,y,z)

  inline :: Point -> Point
  inline (MkPoint pos vel acc) = MkPoint pos (add' vel acc) acc
    where
      add' (vx, vy, vz) (ax, ay, az) = if not $ aligned (vx, vy, vz) (ax, ay, az) then
          add' (add (vx,vy, vz) (ax, ay, az)) (ax,ay,az)
        else
          (vx,vy,vz)
      add (vx,vy,vz) (ax, ay, az) = (vx + ax, vy + ay, vz + az)
      aligned (vx, vy, vz) (ax, ay, az) = and [aligned' vx ax, aligned' vy ay, aligned' vz az]
      aligned' v a = if (v < 0) == (a < 0) && (v > 0) == (a > 0)
        then
          True
        else
          False

  manhattan :: Vector -> Int
  manhattan (x,y,z) = sum $ map abs [x,y,z]

  assignment1 points = fst $ head $ sortOn (snd) $ zipWith (\i x -> ((i, manhattan $ acceleration x))) [0..] points

  step = collide . tick

  tick :: [Point] -> [Point]
  tick = map go
    where
      go (MkPoint pos vel acc) = MkPoint (pos + vel + acc) (vel + acc) acc

  collide :: [Point] -> [Point]
  collide = concat . filter (\x -> length x == 1) . groupBy (\x y -> position x == position y) . (sortOn position)

  assignment2 points = length $ foldr ($) points $ replicate 10000 step


  instance Puzzle.Puzzle PuzzleType where
    parseInput = parseInput
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType
